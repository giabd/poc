terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

provider "random" {
  # Configuration options
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}
